---
title: "Sshd Arch users"
date: 2019-12-05T19:52:35+01:00
itemurl: "items/sshd-au"
sites: ""
tags: [sshd,aur,archlinux]
draft: false
---
Archlinux utilise systemd par defaut :
```text
systemctl enable sshd.service
systemctl start sshd.service
```
