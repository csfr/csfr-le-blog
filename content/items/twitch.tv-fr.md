---
title: "Le lancement de codeskunkfr sur twitch."
date: 2019-12-05T14:05:42+01:00
itemurl: "items/twitch.tv-fr"
sites: "twitch.tv"
tags: [stream,livecoding]
draft: false
---
Lancement de la version exclusivement en francais de codeskunk.
Pas possible de taper des accents pour l'instant avec un layout us sur ma
distribution archlinux.

Je stream sur un vieux macbook pro retina qui n'a plus d'ecran depuis chez moi.
Le contenu varie :
- cours master d'informatique sorbonne universite mention STL
- logique et representation des connaissances
- bases de donnees avancees
- compilateurs
- programmation fonctionnelle (ocaml)
- side projects (react native, c)
