---
title: "Vim Fugitive"
date: 2019-12-05T19:12:01+01:00
itemurl: "items/vim-fugitive"
sites: ""
tags: [github,vim,fr]
draft: false
---

Prerequis: vim-plug ou maitrise paquetage/bundle vim.

Go : [tpope/vim-fugitive](https://github.com/tpope/vim-fugitive)

Avec vim-plug:
```text
if has('nvim')
	call plug#begin(stdpath('data') . '/plugged')
" ...
	Plug 'tpope/vim-fugitive'
" ...
	call plug#end()
endif
```
