# Site CSFR Le Blog

## Reproduire

Instructions en Anglais : http://immense-hollows-6319.herokuapp.com/

## Deploiement

```
git add public
git add config.toml
git push heroku master
```

## Infos

```
hugo
```

## Ajouter un article

```
hugo new items/article.md
```
